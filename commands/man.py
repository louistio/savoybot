#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import manuals
from git import Git
import re

def man(ctx, message):
    g = Git('./')
    log = g.log('-n1', '--abbrev-commit', '--date=iso')
    version = g.describe()
    date_result = re.findall('\d{4}-\d\d-\d\d', log)
    date = ' '.join(date_result)

    man = '''
Gitlab: <https://gitlab.com/savoyroad/savoybot>
```
Welcome to SavoyBot! Below are the available commands
For usage and options, type ".s $cmd --help or -h" (e.g. .s sponge --help)
.s sponge  :   Randomly alternates capitalization
.s pasta   :   Outputs chosen copypasta
.s weather :   Shows weather for desired location
.s ddg     :   Searches DuckDuckGo
.s save    :   Saves the specified user setting e.g. location
.s fm      :   Shows LastFM stats
.s clap    :   Places a clap emoji between each word

Version: {} -- {}
```'''.format(version, date)
    return man
