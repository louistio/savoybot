#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import re
import subprocess
import manuals
import options
import config

def ddg(ctx, message):
    '''Outputs search results for given search term with DDG.'''
    if message[0] in options.helps:
        return manuals.hddg

    elif options.lucky == message[0]:
        if message[1] == options.site:
            web = message[2]
            if web in options.sites:
                web = options.sites[web]
            search = ' '.join(message[3:])
            proc = subprocess.Popen(
                ['ddgr', '--num=1', '--json', '-w', web, search],
                stdout=subprocess.PIPE, universal_newlines=True
            )
            results = proc.stdout.read()
            results = re.sub(
                '^', '`{0}` results for: `{1}`'
                .format(''.join(web), (search)), results
            )
            results = re.sub('^\s+', '', results, flags=re.MULTILINE)
            results = re.sub('\[', '', results)
            results = re.sub('\]', '', results)
            results = re.sub('(.*abstract.*)', r'\n\1\n', results)
            results = re.sub('((\{|\})\n|\},\n)', '', results)
            results = re.sub('"\w{3,8}": ', '', results)
            results = re.sub('",*', '', results)
            return results
        else:
            luck_flag = ' '.join(message[0])
            search = ' '.join(message[1:])
            proc = subprocess.Popen(
                ['ddgr', '--num=1', '--json', luck_flag, search],
                stdout=subprocess.PIPE, universal_newlines=True
            )
            results = proc.stdout.read()
            results = re.sub(
                '^', '`DuckDuckGo` results for: `{0}`'
                .format(' '.join(search)), results
            )
            results = re.sub('^\s+', '', results, flags=re.MULTILINE)
            results = re.sub('\[', '', results)
            results = re.sub('\]', '', results)
            results = re.sub('((\{|\})\n|\},\n)', '', results)
            results = re.sub('(.*abstract.*)', r'\n\1\n', results)
            results = re.sub('"\w{3,8}": ', '', results)
            results = re.sub('",*', '', results)
            return results

    else:
        if message[0] == options.site:
            web = message[1]
            if web in options.sites:
                web = options.sites[web]
            search = ' '.join(message[2:])
            proc = subprocess.Popen(
                ['ddgr', '--json', '-w', web, search],
                stdout=subprocess.PIPE, universal_newlines=True
            )
            results = proc.stdout.read()
            results = re.sub(
                '^', '`{0}` results for: `{1}`'
                .format(''.join(web), (search)), results
            )
            results = re.sub('^\s+', '', results, flags=re.MULTILINE)
            results = re.sub('\[', '', results)
            results = re.sub('\]', '', results)
            results = re.sub('((\{|\})\n|\},\n)', '', results)
            results = re.sub('.*abstract.*', '', results)
            results = re.sub('"\w{3,8}": ', '', results)
            results = re.sub('",*', '', results)
            results = re.sub(
                '((http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))'
                '([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?)', r'<\1>', results
            )
            return results
        else:
            search = ' '.join(message)
            proc = subprocess.Popen(
                ['ddgr', '--json', search],
                stdout=subprocess.PIPE,
                universal_newlines=True
            )
            results = proc.stdout.read()
            results = re.sub(
                '^', '`DuckDuckGo` results for: `{0}`'
                .format(' '.join(message)), results
            )
            results = re.sub('^\s+', '', results, flags=re.MULTILINE)
            results = re.sub(
                '(^(\[|\])|\[$)', '', results, flags=re.MULTILINE
            )
            results = re.sub(
                '((\{|\})\n|\},\n)', '', results, flags=re.MULTILINE
            )
            results = re.sub('.*abstract.*', '', results, flags=re.MULTILINE)
            results = re.sub(
                '(^"\w{3,8}": "|",*$)', '', results, flags=re.MULTILINE
            )
            results = re.sub(
                '((http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))'
                '([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?)', r'<\1>', results
            )
            return results

