#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import inflect
import manuals
import options

p = inflect.engine()

def pasta(ctx, message):
    ''' Prints the copypasta while replacing keywords with user-supplied
    arguments.'''
    arg = message[0]

    if arg in options.helps:
        return manuals.hpasta

    elif arg in options.pasta:
        output = options.pasta[arg].replace('ARG1', message[1])
        try:
            output = output.replace('ARG2', message[2])
        except IndexError:
            pass
        output = output.replace('ARG3', message[1].upper())
        output = output.replace('ARG4', p.plural(message[1]))
        output = output.replace('ARG5', message[1]+"'s")
        return output

    else:
        return manuals.herror
