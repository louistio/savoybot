#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import pylast
import sqlite3
import config
import discord
import manuals
import options

import sql_init
sql = sql_init.SQLInit()

network = pylast.LastFMNetwork(
    api_key=config.key, api_secret=config.secret, username=config.user
)

def fm(ctx, message):
    '''Shows LastFM user info.'''
    try:
        try:
            if message[0] in options.helps:
                return manuals.hfm
            elif ctx.message.mentions:
                user_id = (ctx.message.mentions[0].id,)
                sql.cur.execute("select lastfm from lastfm where id = ?", user_id)
                results = sql.cur.fetchall()
                fm = results[0][0]
                user = network.get_user(fm)
            elif message == ('-v',):
                user_id = (ctx.message.author.id,)
                sql.cur.execute("select lastfm from lastfm where id = ?", user_id)
                results = sql.cur.fetchall()
                fm = results[0][0]
                user = network.get_user(fm)
            else:
                if message and message[0] == '-v':
                    user = network.get_user(' '.join(message[1:]))
                else:
                    user = network.get_user(' '.join(message))
        except IndexError:
            if ctx.message.mentions:
                pass
            else:
                user_id = (ctx.message.author.id,)
                sql.cur.execute("select lastfm from lastfm where id = ?", user_id)
                results = sql.cur.fetchall()
                fm = results[0][0]
                user = network.get_user(fm)

        np = user.get_now_playing()
        recent = user.get_recent_tracks(limit=2)
        total = "{:,}".format(user.get_playcount())

        if np is None:
            playback = recent[0].playback_date
            prev_playback = recent[1].playback_date
            status = 'Most Recent\n{}'.format(playback)
            track = recent[0].track.title
            prev_track = recent[1].track.title
            artist = recent[0].track.artist.name
            prev_artist = recent[1].track.artist.name
            album = recent[0].album
            prev_album = recent[1].album
            art = network.get_album(artist, album)
            art_url = art.get_cover_image()
            playing = '{} - {} [{}]'.format(track, artist, album)
            prev_playing = '{} - {} [{}]'.format(prev_track, prev_artist,
                                                 prev_album)
        else:
            status = 'Currently Scrobbling'
            prev_playback = recent[0].playback_date
            track = np.title
            prev_track = recent[0].track.title
            artist = np.artist.name
            prev_artist = recent[0].track.artist.name
            prev_album = recent[0].album
            art = network.get_artist(artist)
            art_url = art.get_cover_image()
            playing = '{} - {}'.format(track, artist)
            prev_playing = '{} - {} [{}]'.format(prev_track, prev_artist,
                                                 prev_album)


        embed = discord.Embed(color=0xb294bb)
        embed.set_thumbnail(url=art_url)
        embed.set_footer(text='Total scrobbles: {}'.format(total))
        embed.set_author(name=user, url='https://last.fm/user/{}'.format(user)
                         )
        embed.add_field(name='{}'.format(status),
                        value=playing,
                        inline=False
                        )
        if message and message[0] == '-v':
            week = user.get_top_artists(period="7day", limit=10)
            recent_list = []
            for i in week:
                recent_list.append((i.item.name, i.weight))
            recent = []
            for row in recent_list:
                recent.append(" - ".join(row))
            recent = '\n'.join(recent)

            embed.add_field(name='Last Week',
                            value=recent,
                            inline=False
                            )
        else:
            embed.add_field(name='Previous Song',
                            value=prev_playing,
                            inline=False
                            )
        return embed

    except (UnboundLocalError, sqlite3.DatabaseError):
        if message[0] in options.helps:
            pass
        else:
            return manuals.hdberror
