#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
# 
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import options
import text

helps = ['-h', '--help']

pasta = {
        '-l'        :   text.linux,
        '--linux'   :   text.linux,
        '-a'        :   text.apple,
        '--apple'   :   text.apple,
        '-c'        :   text.cummies,
        '--cummies' :   text.cummies,
        '-g'        :   text.good,
        '--good'    :   text.good,
        '-d'        :   text.dicks,
        '--dicks'   :   text.dicks
        }

sites = {
        'urban' :   'urbandictionary.com',
        'wiki'  :   'wikipedia.com',
        'a'     :   'amazon.com',
        'yt'    :   'youtube.com',
        'd'     :   'dictionary.com',
        't'     :   'thesaurus.com'
        }
lucky = '-j'
site = '-w'

units = {
    'm'     :   'celsius',
    'u'     :   'fahrenheit'
}

saves = {
        'location'    :   '-l',
        'lastfm'      :   '-f'
        }

says = {
        '-t'    :   text.tux,
        '-v'    :   text.vader,
        '-m'    :   text.mutilated,
        '-s'    :   text.snowman,
        '-w'    :   text.sodomy
        }

emoji = {
    'smirk'         :   '😏',
    'thinksmirk'    :   '<:thinksmirk:246479897503989760>',
    'duck'          :   '🦆'
}
