#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
# 
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

linux = """
I'd just like to interject for moment. What you're refering to
as ARG2, is in fact, ARG1/ARG2, or as I've recently taken to calling it, ARG1
plus ARG2. ARG2 is not an operating system unto itself, but rather another
free component of a fully functioning ARG1 system made useful by the ARG1
corelibs, shell utilities and vital system components comprising a full OS as
defined by POSIX.

Many computer users run a modified version of the ARG1 system every day,
without realizing it.Through a peculiar turn of events, the version of
ARG1 which is widely used today is often called ARG2, and many of its users
are not aware that it is basically the ARG1 system, developed by the ARG1 Project.
There really is a ARG2, and these people are using it, but it is just a part
of the system they use. ARG2 is the kernel: the program in the system that
allocates the machine's resources to the other programs that you run.

The kernel is an essential part of an operating system, but useless by itself;
it can only function in the context of a complete operating system. ARG2 is
normally used in combination with the ARG1 operating system: the whole system
is basically ARG1 with ARG2 added, or ARG1/ARG2. All the so-called ARG2
distributions are really distributions of ARG1/ARG2!
"""

apple = """
I am quitting Discord chat. I am quitting this chat for good. All day long I
have to deal with ARG1 fangays who think that ARG1 is actually a company
that's worth the dogshit that is stuck on my shoe from 7 months ago. Now that
my ARG1 stock has gone to garbage, I have no reason to remain on this Discord
chat. I must quit Discord chat and ARG1 is to blame.
"""

cummies = '''
IM DELETING YOU, ARG3!😭👋
██]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]] 10% complete.....
████]]]]]]]]]]]]]]]]]]]]]]]]]]] 35% complete....
███████]]]]]]]]]]]]]]]] 60% complete....
███████████] 99% complete.....

🚫ERROR!🚫 💯True💯 ARG4 are irreplaceable 💖I could never delete you
ARG3!💖 Send this to ten other 👪ARG4👪 who give you 💦cummies💦 Or never
get called ☁️squishy☁️ again❌❌😬😬❌❌

If you get 0 Back: no cummies for you 🚫🚫👿
3 back: you're squishy☁️💦
5 back: you're ARG5 kitten😽👼💦
10+ back: ARG1😛😛💕💕💦👅👅
'''

good = '''
👌👀👌👀👌👀👌👀👌👀  ARG1 ARG2 ARG1 ARG2👌 thats ✔ some ARG1👌👌ARG2
right👌👌there👌👌👌 right✔there ✔✔if i do ƽaү so my self 💯 i say so 💯 thats
what im talking about right there right there (chorus: ʳᶦᵍʰᵗ ᵗʰᵉʳᵉ) mMMMMᎷМ💯
👌👌 👌НO0ОଠOOOOOОଠଠOoooᵒᵒᵒᵒᵒᵒᵒᵒᵒ👌 👌👌 👌 💯 👌 👀 👀 👀 👌👌ARG1 ARG2
'''

dicks = '''
ARG1 are so cute omg(⁄ ⁄•⁄ω⁄•⁄ ⁄)⁄ when you hold one in your hand and it
starts twitching its like its nuzzling you(/ω＼) or when they perk up and look
at you like" owo n ya? :3c" hehe ~ ARG1!-kun is happy to see me!!（＾ワ＾）
and the most adorable thing ever is when sperm-sama comes out but theyre rlly
shy so u have to work hard!!(๑•̀ㅁ•́๑)✧ but when ARG1-kun and sperm-sama
meet and theyre blushing and all like "uwaaa~!" (ﾉ´ヮ´)ﾉ: ･ﾟhehehe~ARG1!-kun is
so adorable (●´Д｀●)・
'''

cow = r'''```
 ______
< ARG1 >
 ------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```'''
tux = r'''```
 ______
< ARG1 >
 ------
   \
    \
        .--.
       |o_o |
       |:_/ |
      //   \ \
     (|     | )
    /'\_   _/`\
    \___)=(___/
```'''

vader = r'''```

 ______
< ARG1 >
 ------
        \    ,-^-.
         \   !oYo!
          \ /./=\.\______
               ##        )\/\
                ||-----w||
                ||      ||
```'''

mutilated = r'''```
 ______
< ARG1 >
 ------
       \   \_______
 v__v   \  \   O   )
 (oo)      ||----w |
 (__)      ||     ||  \/\
```'''

snowman = r'''```
 ______
< ARG1 >
 ------
   \
 ___###
   /oo\ |||
   \  / \|/
   /""\  I
()|    |(I)
   \  /  I
  /""""\ I
 |      |I
 |      |I
  \____/ I
```'''

sodomy = r'''```
 ______
< ARG1 >
 ------
  \                 __
   \               (oo)
    \              (  )
     \             /--\
       __         / \  \
      UooU\.'@@@@@@`.\  )
      \__/(@@@@@@@@@@) /
           (@@@@@@@@)((
           `YY~~~~YY' \\
            ||    ||   >>
```'''
