#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
# 
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import socket

# Information
__author__ = ''
__gitlab__ = ''

# Database
db = ''

# Bot Config
if socket.gethostname() == '':
    token = ''
else:
    token = ''

# Administration
root = [] # Author ID
if socket.gethostname() == '':
    bot_id = '<@!>'
else:
    bot_id = '<@!>'
vps_host = ''

# LastFM
key = ''
secret = ''
user = ''

# OpenWeatherMap
wapi = ''
