#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3
import config

class SQLInit:
    def __init__(self):
        self.conn = sqlite3.connect(config.db)
        self.cur = self.conn.cursor()
        self.cur.execute('''
                CREATE TABLE IF NOT EXISTS weather(
                id text primary key not null,
                name text not null,
                unit text,
                location text);
                ''')
        self.conn.commit()
        self.conn.execute('''
                CREATE TABLE IF NOT EXISTS lastfm(
                id text primary key not null,
                name text not null,
                lastfm text);
                ''')
        self.conn.commit()
        self.conn.execute('''
                CREATE TABLE IF NOT EXISTS block(
                id text primary key not null,
                name text not null,
                status integer not null);
                ''')
        self.conn.commit()
